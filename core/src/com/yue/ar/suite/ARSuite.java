package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class ARSuite extends ApplicationAdapter {

	public static final float WORLD_WIDTH = 480;
	public static final float WORLD_HEIGHT = 800;
	// 舞台
	private Stage stage;

	// 纹理
	private Texture texture;

	// 图片控件
	private Image image;
	
	@Override
	public void create () {

		stage = new Stage(new StretchViewport(WORLD_WIDTH, WORLD_HEIGHT));
		// 创建为纹理
		texture = new Texture(Gdx.files.internal("badlogic.jpg"));

		// 创建 Image
		image = new Image(new TextureRegion(texture));

		// 设置 image 的相关属性
		image.setPosition(100, 200);
		image.setScale(0.5F, 1.0F);
		image.setRotation(-5);

		// 添加 image 到舞台
		stage.addActor(image);
	}

	@Override
	public void render () {
		// 黑色清屏
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// 更新舞台逻辑
		stage.act();
		// 绘制舞台
		stage.draw();
	}
	
	@Override
	public void dispose () {
		// 当应用退出时释放资源
		if (texture != null) {
			texture.dispose();
		}
		if (stage != null) {
			stage.dispose();
		}
	}
}
